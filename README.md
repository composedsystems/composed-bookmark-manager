# README #

**composed-bookmark-manager** is an improved implementation of the shipping LabVIEW Bookmark Manager suitable for use on larger projects with higher performance and filtering capabilities. 

## Features and Improvements
- **Added the ability to exclude project VIs by directory** (relative to project root) from the bookmark search.  Useful when external code in project has bookmarks that aren't interesting.  Exclusions are saved on a per-project basis and can be edited through a simple UI.
- **Added the ability to open a project using the browse button**.  After opening, it will automatically change the ring control to the newly opened project and display all bookmarks.
- **Improved the performance of the bookmark manager by 5-10x**.  The main bottleneck was the method that obtained the information about every VI in the project.  Added a cache to avoid doing this operation multiple times, and added disk persistence on a per-project basis to shorten initial load times.  This full scan can be forced by using the new 'Update' button
- Cleaned up the populate tree code, created a few subvis, set VI properties to enable inlining, removed invariant code from loops, etc.

### Performance Comparison
Performance comparison was performed on a LabVIEW project with ~4000 VIs and ~100s of bookmarks.  Actual performance may vary based on your LabVIEW project.  Let me know how it works for you!

#default Bookmark Manager
initial load - 30.084 s
switch project - 15.35 s
warm relaunch - 15.402 s
updates (edit a bookmark in a VI) - 5.31
Grouping change (by bookmark/by VI) - 5.79 

#Composed Bookmark Manager
initial load - 3.379 s
switch project - 3.982 s
warm relaunch - 3.715 s
updates - 0.692
grouping change - 0.58 to 0.63
(new feature) rescan project for new files - 1.419 s

## Portability
This package should work as far back as LabVIEW 2013.
The package build spec would need to be updated to point to the correct directory for the new LabVIEW version.


### Contribution guidelines
- The author is open to pull requests 

### Who do I talk to?
- Steve Ball | Composed Systems, LLC
- steve.ball@composed.io

### License
- See license file